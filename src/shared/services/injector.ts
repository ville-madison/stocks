import {
	Injectable,
	Injector as AngularInjector
} from '@angular/core';

@Injectable()
export class Injector {

	public static injector: AngularInjector;

	constructor(
		injector: AngularInjector
	) {
		Injector.injector = injector;
	}

	/** Get's and injects the given module */
	public static inject(module) {
		return Injector.injector.get(module);
	}
}
