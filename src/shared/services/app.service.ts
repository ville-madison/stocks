import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injector} from './injector';

@Injectable()
export class AppService {

	public http: HttpClient;

	constructor() {
		if (! this.http) {
			this.http = Injector.inject(HttpClient);
		}
	}

	public options(params: HttpParams = null, customHeaders: {} = {}): {[key: string]: any, headers: HttpHeaders} {
		return {
			...params,
			headers: new HttpHeaders({
				'Content-type': 'application/json',
				...customHeaders
			})
		};
	}
}
