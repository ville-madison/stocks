import {animate, animateChild, query, stagger, style, transition, trigger} from '@angular/animations';

export const AEnterFadeX = [
	trigger('list', [
		transition(':enter', [
			query('@item',
				stagger(100, animateChild())
			)
		])
	]),
	trigger('item', [
		transition(':enter', [
			style({ transform: 'translateX(-100px)', opacity: 0 }),
			animate('300ms ease',
				style({ transform: 'translateX(0px)', opacity: 1 }))
		])
	])
];
