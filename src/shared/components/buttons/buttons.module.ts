import {CommonModule} from '@angular/common';
import {ButtonComponent} from './button.component';
import {NgModule} from '@angular/core';

@NgModule({
	declarations: [
		ButtonComponent
	],
	imports: [
		CommonModule
	],
	exports: [
		ButtonComponent
	]
})
export class ButtonsModule {}
