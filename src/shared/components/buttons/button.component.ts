import {
	Component,
	Input,
	OnDestroy,
	OnInit
} from '@angular/core';

@Component({
	selector: 'app-button',
	styles: [],
	template:
		`
	<button
		[type]="type"
		[ngStyle]="style"

		aria-label="Action">
		<ng-content></ng-content>
	</button>`
})
export class ButtonComponent implements OnInit, OnDestroy {

	@Input('color') public color: string;
	@Input('style') public extraStyle: {};
	@Input('type') public type: string = 'button';

	public style: {} = {};

	constructor() {}

	public ngOnInit(): void {
		if (! this.color) {
			return;
		}

		this.style = {
			color: `var(--${this.color})`, // Ex: {color: var(--primary)}
			'border-color': `var(--${this.color})`
		};

		if (this.extraStyle) {
			this.style = {
				...this.style,
				...this.extraStyle
			};
		}
	}

	public ngOnDestroy(): void {}
}
