import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BootModule} from './boot.module';
import {StockModule} from './components/stock/stock.module';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BootModule,

		// Pages
		StockModule
	],
	bootstrap: [
		AppComponent
	]
})
export class AppModule {}
