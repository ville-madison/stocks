import {IStockResponse} from './i.stock.response';

export interface IStock extends IStockResponse {
	currentPrice: number;
	change: number;
	fluctuation: number;
}
