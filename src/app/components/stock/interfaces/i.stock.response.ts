export interface IStockResponse {
	name: string;
	symbol: string;
	price: number;
}
