import {IStock} from '../interfaces/i.stock';

export class StockCache {

	public static day: number;
	public static stocks: {[key: string]: IStock} = {};

	public static hasCache(stock: IStock): boolean {
		return !!StockCache.stocks[ StockCache.getKey(stock) ];
	}

	public static get(stock: IStock): IStock {
		return StockCache.stocks[ StockCache.getKey(stock) ];
	}

	public static remove(stock: IStock): void {
		try {
			delete StockCache.stocks[ StockCache.getKey(stock) ];
		} catch {}
	}

	public static cache(stock: IStock): void {
		StockCache.stocks[ StockCache.getKey(stock) ] = {...stock};
	}

	public static clear(): void {
		StockCache.stocks = {};
	}

	private static getKey(stock: IStock): string {
		return `${stock.symbol}_${StockCache.day}`;
	}
}
