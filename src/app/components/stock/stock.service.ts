import {AppService} from '../../../shared/services/app.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export class StockService extends AppService {

	public getStocks(): Observable<object> {
		return this.http.get(`https://staging-api.brainbase.com/stocks.php`, this.options());
	}
}
