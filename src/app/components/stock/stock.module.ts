import {ModulesModule} from '../../../shared/modules/modules.module';
import {StockService} from './stock.service';
import {StockComponent} from './stock.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
	{ path: '', component: StockComponent}
];

@NgModule({
	declarations: [
		StockComponent
	],
	imports: [
		RouterModule.forChild(routes),
		ModulesModule
	],
	providers: [
		StockService
	]
})
export class StockModule {}
