import {Component, OnInit} from '@angular/core';
import {StockService} from './stock.service';
import {AEnterFadeX} from '../../../shared/animations/a.enter.fade.x';
import {IStock} from './interfaces/i.stock';
import {IStockResponse} from './interfaces/i.stock.response';
import {StockCache} from './classes/stock.cache';

@Component({
	selector: 'app-stock',
	templateUrl: './stock.component.html',
	styleUrls: ['./stock.component.scss'],
	animations: AEnterFadeX
})
export class StockComponent implements OnInit {

	public stocks: IStock[] = [];

	public date: Date = new Date();
	public day: number = 1;

	constructor(
		private service: StockService
	) {}

	public ngOnInit(): void {
		this.getStocks();
	}

	public nextDay(): void {
		this.updateDayAndDate(1);

		this.updateStocks();
	}

	public previousDay(): void {
		this.updateDayAndDate(-1);

		if (this.day <= 1) {
			this.day = 1;
			return this.getStocks();
		}

		this.updateStocks();
	}

	public getStocks(): void {
		this.stocks = [];
		StockCache.clear();

		this.service.getStocks().subscribe( (res: IStockResponse[]) => {
			this.prepareStocks(res);
		});
	}

	private prepareStocks(stocks: IStockResponse[]): void {
		this.stocks = [];

		stocks.forEach( (stock: IStockResponse) => {
			this.stocks.push({
				currentPrice: stock.price,
				fluctuation: 0,
				change: 0,
				...stock
			});
		});
	}

	private updateStocks(): void {
		this.stocks.forEach( (stock: IStock) => {
			if (StockCache.hasCache(stock)) {
				const stockCached = StockCache.get(stock);

				stock.currentPrice = stockCached.currentPrice;
				stock.change = stockCached.change;
				stock.fluctuation = stockCached.fluctuation;
				return StockCache.remove(stock);
			}

			const fluctuation = this.getFluctuation();

			stock.currentPrice = parseFloat(
				(
					stock.currentPrice - (stock.currentPrice * (fluctuation / 100) )
				).toFixed(2)
			);
			stock.change = parseFloat(
				(
					stock.currentPrice - stock.price
				).toFixed(2)
			);
			stock.fluctuation = parseFloat(
				(
					(stock.change * 100) / stock.price
				).toFixed(3)
			);

			StockCache.cache(stock);
		});
	}

	private getFluctuation(): number {
		const direction = this.getFluctuationDirection(),
			fluctuation = Math.floor((Math.random() * 10) + 1);

		return direction ? Math.abs(fluctuation) : -Math.abs(fluctuation);
	}

	private getFluctuationDirection(): number {
		return Math.floor(Math.random() * 2);
	}

	private updateDayAndDate(nr: number): void {
		this.day += nr;
		StockCache.day = this.day;
		this.date = new Date( this.date.setDate(this.date.getDate() + nr) );
	}
}
