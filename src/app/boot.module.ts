import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {AppService} from '../shared/services/app.service';
import {Injector} from '../shared/services/injector';
import {AppRouteModule} from './app.route.module';

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		AppRouteModule,
		HttpClientModule,
	],
	providers: [
		Injector,
		AppService
	],
	exports: [
		BrowserModule,
		BrowserAnimationsModule,
		AppRouteModule,
		HttpClientModule
	]
})
export class BootModule {

	/** Start singletons */
	constructor(
		private injector: Injector,
		private appService: AppService
	) {}
}
